/**
 * Created by nicky on 3/08/2016.
 */
var WeatherWidget = (function() {
    // Private variables
    var _id, _container, _url = "http://datatank.stad.gent/4/milieuennatuur/weersomstandigheden.json", _data;

    // Constructor
    WeatherWidget.prototype.constructor = WeatherWidget;
    function WeatherWidget(id, container) {
        _id = id;
        _container = container;
    }

    // Method: toString
    // String representation of object
    WeatherWidget.prototype.toString = function() {
        return "Weather Widget with id " + _id;
    };

    // Method: toString
    // String representation of object
    WeatherWidget.prototype.loadData = function() {

        var self = this;
        Utils.getJSONByPromise(_url).then(
            function(data) {
                console.log(data);
                _data = data;
                self.update();
            },
            function(status) {
                console.log(status);
            }
        );

    };

    // Method: update
    WeatherWidget.prototype.update = function() {

        var attributes = _data.properties.attributes;


        _container.querySelector('.now__temp').innerHTML = attributes[0].value + '°C';
        _container.querySelector('.now__info').innerHTML = attributes[7].value;


    };

    return WeatherWidget;

})();