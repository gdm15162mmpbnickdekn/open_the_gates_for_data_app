/*
*	Title: Worldbank Application
*	Modified: 19-11-2015
*	Version: 1.0.0
*	Author: Philippe De Pauw - Waterschoot
* 	-----------------------------------------------
*	Worldbank countries API: http://api.worldbank.org/countries/all?format=jsonP&prefix=jsonp_callback&per_page=300
* 
*   ?([{"page":1,"pages":1,"per_page":"300","total":264},[{"id":"ABW","iso2Code":"AW","name":"Aruba","region":{"id":"LCN","value":"Latin America & Caribbean (all income levels)"},"adminregion":{"id":"","value":""},"incomeLevel":{"id":"NOC","value":"High income: nonOECD"},"lendingType":{"id":"LNX","value":"Not classified"},"capitalCity":"Oranjestad","longitude":"-70.0167","latitude":"12.5167"}
*/

// Anonymous function executed when document loaded
(function() {
	
	// Describe an App object with own functionalities
	var App = {
		init: function() {
			this.WBCOUNTRIESAPIURL = "http://datatank.stad.gent/4/toerisme/visitgentevents.json";


			
			this.registerNavigationToggleListeners();// Register All Navigation Toggle Listeners
			
			this.registerWindowListeners();// Register All Navigation Toggle Listeners
			
			// Register listeners for list layout
			this.registerListenersForListLayout();

			this.createContactForm();//contact js

		},
		registerNavigationToggleListeners: function() {
			var toggles = document.querySelectorAll('.navigation-toggle');
			
			if(toggles != null && toggles.length > 0) {
				var toggle = null;
				
				for(var i = 0; i < toggles.length; i++ ) {
					toggle = toggles[i];
					toggle.addEventListener('click', function(ev) {
						ev.preventDefault();
						
						document.querySelector('body').classList.toggle(this.dataset.navtype);
						
						return false;
					});	
				}
			}
		},
		registerWindowListeners: function() {
			window.addEventListener('resize', function(ev) {
				if(document.querySelector('body').classList.contains('offcanvas-open')) {
					document.querySelector('body').classList.remove('offcanvas-open');
				}
				
				if(document.querySelector('body').classList.contains('headernav-open')) {
					document.querySelector('body').classList.remove('headernav-open');
				}
			});
		},
		registerListenersForListLayout: function() {
			
			var self = this;
			
			var anchors = document.querySelectorAll('[data-listlayout]');
			if(anchors != null && anchors.length > 0) {
				_.each(anchors, function(anchor) {
					anchor.addEventListener('click', function(ev) {
						ev.preventDefault();
						
						var layout = this.dataset.listlayout;
						var target = this.dataset.target;
						
						if(target == 'countries-list') {
							self.updateCountriesUI('countries-' + layout, '#countries-' + layout + '-template');// Call updateCountriesUI method when successful
						}
						
						return false;
						console.log(ev);	
					});	
				});
			}
		},
		createContactForm: function() {
			// Get the form.
			var form = $('#contact_form');
			// Get the messages div.
			var formMessages = $('#message');
			// Set up an event listener for the contact form.
			$(form).submit(function(e) {
				// Stop the browser from submitting the form.
				e.preventDefault();
				// Serialize the form data.
				var formData = $(form).serialize();
				// Submit the form using AJAX.
				$.ajax({
						type: 'POST',
						url: $(form).attr('action'),
						data: formData
					})
					.done(function(response) {
						// Make sure that the formMessages div has the 'success' class.
						$(formMessages).removeClass('error');
						$(formMessages).addClass('success');
						// Set the message text.
						$(formMessages).text(response);
						// Clear the form.
						$('#name').val('');
						$('#email').val('');
						$('#subject').val('');
						$('#message').val('');
					})
					.fail(function(data) {
						// Make sure that the formMessages div has the 'error' class.
						$(formMessages).removeClass('success');
						$(formMessages).addClass('error');
						// Set the message text.
						if (data.responseText !== '') {
							$(formMessages).text(data.responseText);
						} else {
							$(formMessages).text('Oops! Er is iets mis gelopen probeer opnieuw.');
						}
					});
			});
		}
	};
	
	App.init();// Intialize the application

    var weatherWidget1 = new WeatherWidget(0, document.querySelector(".weather-widget"));
    weatherWidget1.loadData();

    //sport

    var Sport = {
        "init" : function() {

            this.URLDSGHENTSPORTLOCATIONS = 'http://datatank.stad.gent/4/cultuursportvrijetijd/buurtsportlocaties.json';// Cache the url with sportlocations in variable URLDSGHENTSPORTLOCATIONS
            this._ghentSportlocationsData = null;// Cached data fro sportlocations in Ghent
            this.loadGhentSportlocations();// Callback: Load dataset Sportlocations from Ghent via API

        },
        "loadGhentSportlocations": function() {

            // Closure
            var self = this;

            // Load JSON from corresponding API with certain URL
            Utils.getJSONByPromise(this.URLDSGHENTSPORTLOCATIONS).then(
                function(data) {
                    self._ghentSportlocationsData = data;// Assign data as value flor global variable _ghentSportlocationsData within the App
                    self.updateUI();// Call updateUI method when successful
                },
                function(status) {
                    console.log(status);
                }
            );

        },
        "updateUI": function() {

            if(this._ghentSportlocationsData != null && this._ghentSportlocationsData.features != null) {
                // Soring the data on description of the parking
                this._ghentSportlocationsData.features.sort(function(a, b) {
                    if (a.properties.Naam > b.properties.Naam) {
                        return 1;
                    }
                    if (a.properties.Naam < b.properties.Naam) {
                        return -1;
                    }
                    return 0;
                });
                var tempStr = '', sportLocation = null;
                tempStr += '<ul>';
                for(var i = 0;i < this._ghentSportlocationsData.features.length;i++) {
                    sportLocation = this._ghentSportlocationsData.features[i];
                    tempStr += '<li class="sportlocation" data-id="' + sportLocation.properties.NUMMER + '" data-title="' + sportLocation.properties.Sport + '">';
                    tempStr += '<span class="sportlocation__name">' + sportLocation.properties.Naam + '</span>';
                    tempStr += '<span class="sportlocation__district">' + sportLocation.properties.Wijk + '</span>';
                    tempStr += '<span class="sportlocation__sport">' + sportLocation.properties.Sport + '</span>';
                    tempStr += '</li>';
                }
                tempStr += '</ul>';
                document.querySelector('.ghent-sportlocations').innerHTML = tempStr;
            }

        }
    };
    Sport.init();

    //searchbar
    document.querySelector('#txtSearch').addEventListener('keyup', function(ev)           {
        ev.preventDefault();

        var txt = ev.target.value.toLowerCase();

        var countryNodes = document.querySelectorAll('.ghent-sportlocations>ul>li');
        for(var i=0;i<countryNodes.length;i++) {
            var c = countryNodes[i];
            if(c.dataset.title.toLowerCase().indexOf(txt) == -1) {
                c.classList.add('sportlocationHidden');
            } else {
                c.classList.remove('sportlocationHidden');
            }
        }

        return false;
    });

	//searchbar 2
	document.querySelector('#txtSearch2').addEventListener('keyup', function(ev)           {
		ev.preventDefault();

		var txt = ev.target.value.toLowerCase();

		var countryNodes = document.querySelectorAll('.GhentEvent>ul>a>li');
		for(var i=0;i<countryNodes.length;i++) {
			var c = countryNodes[i];
			if(c.dataset.title.toLowerCase().indexOf(txt) == -1) {
				c.classList.add('sportlocationHidden');
			} else {
				c.classList.remove('sportlocationHidden');
			}
		}

		return false;
	});


	//Quiz
	window.onload = function () {

		var questionArea = document.getElementsByClassName('questions')[0],
			answerArea   = document.getElementsByClassName('answers')[0],
			checker      = document.getElementsByClassName('checker')[0],
			current      = 0,

		// An object that holds all the questions + possible answers.
		// In the array --> last digit gives the right answer position
			allQuestions = {
				'Van welke in Gent geboren keizer was Maria van Bourgondië de grootmoeder?' : ['Keizer Karel', 'Keizer Napoleon I', 'Keizer Lodewijk I', 0],

				'Hoe heet de rivier die door Gent stroomt, werd vroeger de gouden rivier genoemd.' : ['De Schelde', 'De Leie' , 'De Dender', 1],

				'In welk jaar is Luc De Vos geboren? ' : ['1960', '1962', '1963', 1]
			};

		function loadQuestion(curr) {
			// This function loads all the question into the questionArea
			// It grabs the current question based on the 'current'-variable

			var question = Object.keys(allQuestions)[curr];

			questionArea.innerHTML = '';
			questionArea.innerHTML = question;
		}

		function loadAnswers(curr) {

			var answers = allQuestions[Object.keys(allQuestions)[curr]];

			answerArea.innerHTML = '';

			for (var i = 0; i < answers.length -1; i += 1) {
				var createDiv = document.createElement('div'),
					text = document.createTextNode(answers[i]);

				createDiv.appendChild(text);
				createDiv.addEventListener("click", checkAnswer(i, answers));


				answerArea.appendChild(createDiv);
			}
		}

		function checkAnswer(i, arr) {

			return function () {
				var givenAnswer = i,
					correctAnswer = arr[arr.length-1];

				if (givenAnswer === correctAnswer) {
					addChecker(true);
				} else {
					addChecker(false);
				}

				if (current < Object.keys(allQuestions).length -1) {
					current += 1;

					loadQuestion(current);
					loadAnswers(current);
				} else {
					questionArea.innerHTML = 'Done';
					answerArea.innerHTML = '';
				}

			};
		}

		function addChecker(bool) {
			// This function adds a div element to the page
			// Used to see if it was correct or false

			var createDiv = document.createElement('div'),
				txt       = document.createTextNode(current + 1);

			createDiv.appendChild(txt);

			if (bool) {

				createDiv.className += 'correct';
				checker.appendChild(createDiv);
			} else {
				createDiv.className += 'false';
				checker.appendChild(createDiv);
			}
		}


		// Start the quiz right away
		loadQuestion(current);
		loadAnswers(current);

	};
	//button

})();
