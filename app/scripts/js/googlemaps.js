/**
 * Created by nicky on 6/08/2016.
 */

(function() {
    // Laad map + Geolocation
    function createMap(mymap) {
        mymap.locate({setView: true, maxZoom: 11});

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18,
            id: 'nickdekn.143ho7pa',
            accessToken: 'pk.eyJ1Ijoibmlja2Rla24iLCJhIjoiY2lydGI5b2VrMDBmYWhwbmg0MmIyZGZ4MyJ9.mHvftQdtoT2WpDd4vk1Acg'
        }).addTo(mymap);

        function onLocationFound(e) {
            var radius = e.accuracy / 2;

            L.marker(e.latlng).addTo(mymap)
                .bindPopup("You are here").openPopup();

            L.circle(e.latlng, radius).addTo(mymap);
        }

        mymap.on('locationfound', onLocationFound);

        function onLocationError(e) {
            alert(e.message);
        }

        mymap.on('locationerror', onLocationError);
    }

    var mapbioscopen = L.map('mapid');
    createMap(mapbioscopen);

    var mapbibliotheken = L.map('mapid2');
    createMap(mapbibliotheken);

    var maphistorischefilm = L.map('mapid3');
    createMap(maphistorischefilm);

    //LoadAttractiePolen
    var _data;

    function loadAttractiePolen() {
        var self = this;
        var _url = "http://datatank.stad.gent/4/cultuursportvrijetijd/bioscopen.geojson";
        Utils.getJSONByPromise(_url).then(
            function(data) {
                console.log(data);
                _data = data;
                updateAttractiepolen();
            },
            function(status) {
                console.log(status);
            }
        );
    }

    function updateAttractiepolen() {
        L.geoJson(_data).addTo(mapbioscopen);
    }
    loadAttractiePolen();

    //bibliotheken inladen

    var _data;

    function loadBibliotheken() {
        var self = this;
        var _url = "http://datatank.stad.gent/4/cultuursportvrijetijd/bibliotheek.geojson";
        Utils.getJSONByPromise(_url).then(
            function(data) {
                console.log(data);
                _data = data;
                updateBibliotheken();
            },
            function(status) {
                console.log(status);
            }
        );
    }

    function updateBibliotheken() {
        L.geoJson(_data).addTo(mapbibliotheken);
    }
    loadBibliotheken();

    //Historischefilm inladen

    var _data;

    function loadHistorischeFilm() {
        var self = this;
        var _url = "http://datatank.stad.gent/4/cultuursportvrijetijd/historischefilmvoorstellingen.geojson";
        Utils.getJSONByPromise(_url).then(
            function(data) {
                console.log(data);
                _data = data;
                updateHistorischeFilm();
            },
            function(status) {
                console.log(status);
            }
        );
    }

    function updateHistorischeFilm() {
        L.geoJson(_data).addTo(maphistorischefilm);
    }
    loadHistorischeFilm();
})();

