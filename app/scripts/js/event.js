/**
 * Created by nicky on 11/08/2016.
 */
(function() {
    var Event = {
        "init": function () {
            var self = this;

            this.URLDSGHENTEVENT = 'http://datatank.stad.gent/4/toerisme/visitgentevents.json';// Cache the url with sportlocations in variable URLDSGHENTSPORTLOCATIONS
            this._dataCountries = null;// Cached data fro sportlocations in Ghent
            this._dataCountry = {
                "info": null
            };

            // Handlebars Cache
            this._hbsCache = {};// Handlebars cache for templates
            this._hbsPartialsCache = {};// Handlebars cache for partials

            this._jayWalker = JayWalker;
            this._jayWalker.init();
            this._jayWalker._countryDetailsJSONPLoaded.add(function(title) {
                self.loadDatasetsFromStreet(title);
            });

            this.loadGhentEvent();// Callback: Load dataset Sportlocations from Ghent via API


        },
        "loadGhentEvent": function () {

            // Closure
            var self = this;

            // Load JSON from corresponding API with certain URL
            Utils.getJSONByPromise(this.URLDSGHENTEVENT).then(
                function (data) {
                    self._dataCountries = data;// Assign data as value flor global variable _ghentSportlocationsData within the App
                    console.log(self._dataCountries);
                    self.updateEventsUI();// Call updateUI method when successful
                },
                function (status) {
                    console.log(status);
                }
            );

        },
        //update event list
        "updateEventsUI": function () {

            if (this._dataCountries != null) {
                var tempStr = '', EventLocation = null;
                tempStr += '<ul>';
                for (var i = 0; i < this._dataCountries.length; i++) {
                    EventLocation = this._dataCountries[i];
                    tempStr += '<a href="#/events/details/' + EventLocation.title +'">';
                    tempStr += '<li class="sportlocation" data-id="' + EventLocation.title + '" data-title="' + EventLocation.title + '">';
                    tempStr += '<span class="sportlocation__name">' + EventLocation.title + '</span>';
                    tempStr += '<span class="sportlocation__district">' + EventLocation.spots[0].title + '</span>';
                    tempStr += '<span class="sportlocation__sport">' + EventLocation.openinghours_short + '</span>';
                    tempStr += '</li>';
                    tempStr += '</a>';
                }
                tempStr += '</ul>';
                document.querySelector('.GhentEvent').innerHTML = tempStr;
            }

        },
        loadDatasetsFromStreet: function(title) {
            var selectedCountry = _.find(this._dataCountries, function(country) {
                return country.title == title;
            });
            if(selectedCountry != null) {
                this._dataCountry.info = selectedCountry;
            }
            this.updateStreetsDetailsUI("street-details", "#event-details-template");// load streetdetails
        },
        updateStreetsDetailsUI: function(hbsTmplName, hbsTmplId) {
            if(!this._hbsCache[hbsTmplName]) {
                var src = document.querySelector(hbsTmplId).innerHTML;// Get the contents from the specified hbs template
                this._hbsCache[hbsTmplName] = Handlebars.compile(src);// Compile the source and add it to the hbs cache
            }
            document.querySelector('.event-detail').innerHTML = this._hbsCache[hbsTmplName](this._dataCountry);// Write compiled content to the appropriate container
        }
    };
    Event.init();
})();