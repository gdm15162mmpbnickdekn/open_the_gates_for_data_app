<?php
/**
 * Created by PhpStorm.
 * User: nicky
 * Date: 9/08/2016
 * Time: 18:34
 */

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        if(isset($_POST["name"])) {
            $name = strip_tags(trim($_POST["name"]));
            $name = str_replace(array("\r", "\n"), array(" ", " "), $name);
            return $name;
        }
        if(isset($_POST["email"])) {
            $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
            return $email;
        }
        if(isset($_POST["subject"])) {
            $subject = trim($_POST["subject"]);
            return $subject;
        }
        if(isset($_POST["message"])) {
            $message = trim($_POST["message"]);
            return $message;
        }
        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! Vul alle velden correct in en probeer opnieuw.";
            exit;
        }
        // Set the recipient email address.
        $recipient = "nickydeknibber@student.arteveldehs.be";
        // Set the email subject.
        $subject = "Nieuw contact van $name";
        // Build the email content.
        $email_content = "Naam: $name\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "Onderwerp: $email\n\n";
        $email_content .= "Bericht:\n$message\n";
        // Build the email headers.
        $email_headers = "Van: $name <$email>";
        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Bedankt! Je bericht is verzonden.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Er ging iets verkeerd, probeer opnieuw.";
        }
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Oops! Er ging iets verkeerd, probeer opnieuw.";
    }
?>
